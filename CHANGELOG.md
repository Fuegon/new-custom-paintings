# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 1.1.0
### Added
- Village painting
- Church painting

## 1.0.0 - 2024-06-15

- Initial release based on the old [Custom Paintings](https://gitlab.com/Fuegon/custom-paintings)

### Added
- River painting
- Forest painting
- Mountain painting
- Road painting
- Port painting
- Bridge painting
- Sunset painting
- Fairy Nebula painting
- Cone Nebula painting
- Flowers painting
- Temple painting
- Sun painting
- Ruins painting
- Pandemonium painting
- Heimweg painting