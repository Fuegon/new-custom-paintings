# New custom paintings resource pack and datapack
This Minecraft resourcepack adds some new paintings by using the  Painting variants feature added in 1.21.

> This is an update of my Custom Paintings resource pack for Minecraft versions 1.14 to 1.20. You can find it [here](https://gitlab.com/Fuegon/custom-paintings). This repo only works for 1.21+ versions of Minecraft.

## Installation
The new painting variants features requires both a resource pack and a datapack.

Copy the resource pack in your Minecraft installation `resourcepacks` folder and the datapack in your world `datapacks` folder.
You have to add the resource pack to the selected packs.

For more information see [datapack usage](https://minecraft.wiki/w/Data_pack#Usage) and [resource pack behavior](https://minecraft.wiki/w/Resource_pack#Behavior) in the Minecraft wiki.

**Note: This datapack will mark your world as using experimental settings.**

## Download
### Modrinth
You can download the resource pack from [Modrinth](https://modrinth.com/resourcepack/new-custom-paintings).

### CurseForge
You can download the resource pack from [CurseForge](https://www.curseforge.com/minecraft/texture-packs/new-custom-paintings).

### This repository
#### Releases
**This is not yet available in this repo**

Visit the [releases page for this repository](https://gitlab.com/Fuegon/new-custom-paintings/-/releases) to get the last released version of the resource pack. 

#### Working version
Download this repository.

Copy each file in the corresponding folder as outlined in the installation section.

## Usage
The paintings are available as any other Minecraft painting. Either randomly selected when placing a painting in the world or with the predetermined painting from the creative inventory.

The next image shows all the available paintings:

![Available paintings](legend.png)

## Credits
First 4 sample paintings in the resource pack are created from art released under CC0 license by ansimuz. You can find the original images at the next links:

- "River" - [Top Down River - Shooter Environment by ansimuz](https://opengameart.org/content/top-down-river-shooter-environment)
- "Forest" - [Forest Background  by ansimuz](https://opengameart.org/content/forest-background)
- "Mountain" - [Mountain at Dusk Background by ansimuz](https://opengameart.org/content/mountain-at-dusk-background)
- "Road" - [Country Side Platform Tiles by ansimuz](https://opengameart.org/content/country-side-platform-tiles)

The rest of the paintings are created based on public domain images. You can find the originals in the next links:

- "Port" - [Cumulus Clouds, East River by Robert Henri](https://commons.wikimedia.org/wiki/File:Cumulus_Clouds,_East_River_SAAM-1992.91_1.jpg)
- "Bridge" - [Florence by Henryk Dietrich](https://artvee.com/dl/florence-3/)
- "Sunset" - [Sunset by Frederic Edwin Church](https://artvee.com/dl/sunset-13/)
- "Fairy Nebula" - [The Fairy of Eagle Nebula by NASA, ESA, and The Hubble Heritage Team (STScI/AURA)](https://commons.wikimedia.org/wiki/File:Fairy_of_Eagle_Nebula.jpg)
- "Cone Nebula" - [Cone Nebula/NGC 2264 by NASA, H. Ford (JHU), G. Illingworth (UCSC/LO), M.Clampin (STScI), G. Hartig (STScI), the ACS Science Team, and ESA](https://commons.wikimedia.org/wiki/File:Cone_Nebula_(NGC_2264)_Star-Forming_Pillar_of_Gas_and_Dust.jpg)
- "Flowers" - [Still Life with Flowers in a Glass Vase by Jan Davidsz. de Heem](https://www.rijksmuseum.nl/en/collection/SK-C-214)
- "Temple" - [brown high-rise building by Lucrezia Carnelos](https://unsplash.com/photos/8XR23JaBUuA)
- "Sun" - [After the Deluge by George Frederic Watts](https://commons.wikimedia.org/wiki/File:Watts_%E2%80%93_After_the_Deluge.jpg)
- "Ruins" - [Chimborazo Volcano by Frederic Edwin Church](https://artvee.com/dl/chimborazo-volcano/)
- "Pandemonium" - [Pandemonium by John Martin](https://commons.wikimedia.org/wiki/File:John_Martin_Le_Pandemonium_Louvre.JPG)
- "Heimweg" - [Auf dem Heimweg II by Anna Klein ](https://commons.wikimedia.org/wiki/File:Anna_Klein_12_Auf_dem_Heimweg_II.jpg)
- "Village" - [Houses and the Church at the Salwator Hill in Krakow by Henryk Dietrich](https://artvee.com/dl/houses-and-the-church-at-the-salwator-hill-in-krakow/)
- "Church" - [The Coronation of Queen Victoria by John Martin](https://commons.wikimedia.org/wiki/File:Coronation_of_Queen_Victoria_-_John_Martin.jpg)

## Acknowledgements
- Minecraft Wiki fantastic and to the point [article](https://minecraft.wiki/w/Painting_variant_definition) and [tutorial](https://minecraft.wiki/w/Tutorials/Adding_custom_paintings) about painting variants.
- [cassiancc's reference repo for painting variants](https://github.com/cassiancc/painting-variant-example/tree/main)
- Hermitcraft for inspiring the original custom paintings resourcepack, as seen in [this video (14:53)](https://youtu.be/OBOAlfX7Zus?t=893).

---
New Custom Paintings © 2024 by Fuegon is licensed under [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/)